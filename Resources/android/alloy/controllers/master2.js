function Controller() {
    function init() {
        espnService.getespn(function(espnData) {
            if (espnData) {
                var headlines = espnData.headlines;
                for (var i = 0; headlines.length > i; i++) {
                    var image;
                    var link;
                    try {
                        image = headlines[i].images[0].url;
                    } catch (ex) {
                        image = "/pariveda-logo.gif";
                    }
                    try {
                        link = headlines[i].links.mobile.href;
                    } catch (ex) {
                        link = "";
                    }
                    var date = headlines[i].published.split("T");
                    var rows = [];
                    rows.push(Alloy.createController("row", {
                        articleUrl: link,
                        image: image,
                        title: headlines[i].headline,
                        date: date[0]
                    }).getView());
                }
            }
        });
        $.table.setData(rows);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "master2";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    exports.destroy = function() {};
    _.extend($, $.__views);
    var espnService = require("espn");
    init();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;