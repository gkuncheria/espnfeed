function Controller() {
    function openDetail(e) {
        $.trigger("detailClick", e);
    }
    function init() {
        espnService.getEspn(function(espnData) {
            if (espnData) {
                var rows = [];
                var headlines = espnData.headlines;
                for (var i = 0; headlines.length > i; i++) {
                    var image;
                    try {
                        image = headlines[i].images[0].url;
                    } catch (ex) {
                        image = "/pariveda-logo.gif";
                    }
                    var date = headlines[i].published.split("T");
                    rows.push(Alloy.createController("row", {
                        articleUrl: headlines[i].links.mobile.href,
                        image: image,
                        title: headlines[i].headline,
                        date: date[0]
                    }).getView());
                }
                $.table.setData(rows);
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "master";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.master = Ti.UI.createWindow({
        backgroundColor: "#fff",
        navBarHidden: true,
        exitOnClose: true,
        title: "Texans Headlines",
        id: "master"
    });
    $.__views.master && $.addTopLevelView($.__views.master);
    $.__views.header = Ti.UI.createLabel({
        width: Ti.UI.FILL,
        height: "50dp",
        color: "#fff",
        textAlign: "center",
        backgroundGradient: {
            type: "linear",
            startPoint: {
                x: "0%",
                y: "0%"
            },
            endPoint: {
                x: "0%",
                y: "100%"
            },
            colors: [ {
                color: "#003366",
                offset: "0.0"
            }, {
                color: "#597498",
                offset: "1.0"
            } ]
        },
        font: {
            fontSize: "24dp",
            fontWeight: "bold"
        },
        text: "Texans Headlines",
        id: "header"
    });
    $.__views.table = Ti.UI.createTableView({
        headerView: $.__views.header,
        id: "table"
    });
    $.__views.master.add($.__views.table);
    openDetail ? $.__views.table.addEventListener("click", openDetail) : __defers["$.__views.table!click!openDetail"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var espnService = require("espn");
    init();
    __defers["$.__views.table!click!openDetail"] && $.__views.table.addEventListener("click", openDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;