function getEspn(onSuccess) {
    var url = "http://api.espn.com/v1/sports/football/nfl/news/?teams=34&apikey=gx4g3twpxt5xu8kt39yw59e9";
    Ti.API.info("Getting Espn: " + url);
    var client = Ti.Network.createHTTPClient({
        onload: function() {
            json = JSON.parse(this.responseText);
            Ti.API.debug(json);
            onSuccess(json);
        },
        onerror: function(e) {
            Ti.API.debug(e.error);
            alert("Could not retrieve current temperature!");
        }
    });
    client.open("GET", url);
    client.send();
}

var Espn = {
    getEspn: function(onSuccess) {
        return getEspn(onSuccess);
    }
};

module.exports = Espn;