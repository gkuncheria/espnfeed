function Controller() {
    function openDetail(e) {
        $.trigger("detail2", e);
    }
    function refreshHeadlines() {
        feed.loadHeadlines({
            success: function(data) {
                var rows = [];
                _.each(data, function(item) {
                    rows.push(Alloy.createController("row", {
                        articleUrl: item.link,
                        image: item.image,
                        title: item.title,
                        date: item.pubDate
                    }).getView());
                });
                $.table.setData(rows);
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "master";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.master = Ti.UI.createWindow({
        backgroundColor: "#fff",
        title: "Texans Headlines",
        id: "master"
    });
    $.__views.master && $.addTopLevelView($.__views.master);
    $.__views.refreshButton = Ti.UI.createButton({
        title: "refresh",
        id: "refreshButton"
    });
    refreshHeadlines ? $.__views.refreshButton.addEventListener("click", refreshHeadlines) : __defers["$.__views.refreshButton!click!refreshHeadlines"] = true;
    $.__views.master.rightNavButton = $.__views.refreshButton;
    $.__views.table = Ti.UI.createTableView({
        id: "table"
    });
    $.__views.master.add($.__views.table);
    openDetail ? $.__views.table.addEventListener("click", openDetail) : __defers["$.__views.table!click!openDetail"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var feed = require("headlines");
    refreshHeadlines();
    __defers["$.__views.refreshButton!click!refreshHeadlines"] && $.__views.refreshButton.addEventListener("click", refreshHeadlines);
    __defers["$.__views.table!click!openDetail"] && $.__views.table.addEventListener("click", openDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;