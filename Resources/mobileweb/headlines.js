var url = "http://api.espn.com/v1/sports/football/nfl/news/?teams=34&apikey=gx4g3twpxt5xu8kt39yw59e9";

exports.loadHeadlines = function(o, tries) {
    var xhr = Titanium.Network.createHTTPClient();
    tries = tries || 0;
    xhr.open("GET", url);
    xhr.onload = function() {
        var json = JSON.parse(this.responseText);
        if (null === json || 0 === json.length) {
            if (3 > tries) {
                tries++;
                exports.loadHeadlines(o, tries);
                return;
            }
            alert("No Headlines!");
            o.error && o.error();
            return;
        }
        var headlines = json.headlines;
        var data = [];
        for (var i = 0; headlines.length > i; i++) {
            var image;
            var link;
            try {
                image = headlines[i].images[0].url;
            } catch (ex) {
                image = "/pariveda-logo.gif";
            }
            try {
                link = headlines[i].links.mobile.href;
            } catch (ex) {
                link = "";
            }
            var date = headlines[i].published.split("T");
            data.push({
                title: headlines[i].headline,
                link: link,
                pubDate: date[0],
                image: image
            });
        }
        o.success && o.success(data);
    };
    xhr.onerror = function() {
        o.error && o.error();
    };
    o.start && o.start();
    xhr.send();
};