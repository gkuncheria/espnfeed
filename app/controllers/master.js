var espnService = require('espn');
// trigger() - http://backbonejs.org/#Events-trigger
function openDetail(e) {
	$.trigger('detailClick', e);
}

function init() {	
		espnService.getEspn(function(espnData){
		if (espnData){
		var rows = [];
		var headlines = espnData.headlines;
		
		for (var i = 0; i < headlines.length; i++) {
			var image;
			
			try {
				image = headlines[i].images[0].url;
			} catch (ex) {
				image = '/pariveda-logo.gif';
			}

			var date = headlines[i].published.split("T");
				//createController method - http://docs.appcelerator.com/titanium/3.0/#!/api/Alloy-method-createController
				//getView method - http://docs.appcelerator.com/titanium/3.0/#!/api/Alloy.Controller-method-getView
				rows.push(Alloy.createController('row', {
					articleUrl: headlines[i].links.mobile.href,
					image: image,
					title: headlines[i].headline,
					date: date[0]
				}).getView());

		}
		//setData method - http://docs.appcelerator.com/titanium/3.0/#!/api/Titanium.UI.DashboardView-method-setData
			$.table.setData(rows);
		}
	});
}

function refresh(){
	init();
}


init();