// respond to detail event triggered on index controller
// on() - http://backbonejs.org/#Events-on
$.master.on('detailClick', function(e) {
	// get the detail controller and window references
	var controller = Alloy.createController('detail');
	var win = controller.getView();

	// set the new detail article
	controller.setArticle(e.row.articleUrl);

	// open the detail windows
	win.open();
});

$.master.getView().open();

